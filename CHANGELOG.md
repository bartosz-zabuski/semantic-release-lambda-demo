# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.1](https://bitbucket.org/bartosz-zabuski/semantic-release-lambda-demo/compare/v1.0.0...v1.0.1) (2020-11-17)


### Bug Fixes

* fake fix ([cd7135f](https://bitbucket.org/bartosz-zabuski/semantic-release-lambda-demo/commits/cd7135f956f8231f93e41537b14ae52cb9f26ac6))

# 1.0.0 (2020-11-17)


### Features

* add Jenkinsfile ([29bbefa](https://bitbucket.org/bartosz-zabuski/semantic-release-lambda-demo/commits/29bbefa991f207d39ae266fbe5cbc0252baaa12b))
