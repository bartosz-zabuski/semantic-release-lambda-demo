import 'source-map-support/register';
import { APIGatewayProxyHandler } from 'aws-lambda';
import { multiply, random } from 'lodash';

export function calculate(){
    return multiply(random(0, 1111111), random(0,22222));
}

export const testEndpoint1: APIGatewayProxyHandler = async (event, _context) => {

    return {
      statusCode: 200,
      body: JSON.stringify({
        message: `testEndpoint1 exectued! ${calculate()}`,
        input: event,
      }, null, 2),
    };
  }
  